SOURCES=main.tex
PDF_OBJECTS=$(SOURCES:.tex=.pdf)

LATEXMK=latexmk
LATEXMK_OPTIONS=-bibtex -pdf -pdflatex="pdflatex -interaction=nonstopmode"

DOCKER=docker
DOCKER_COMMAND=run --rm -w /data/ --env LATEXMK_OPTIONS_EXTRA=$(LATEXMK_OPTIONS_EXTRA)
DOCKER_MOUNT=-v`pwd`:/data

all: clean biber render

tangle:
	-emacs --batch --eval "(require 'org)" --eval '(org-babel-tangle-file "index.org")'

pdf: $(PDF_OBJECTS)

%.pdf: %.tex
	@echo Input file: $<
	$(LATEXMK) $(LATEXMK_OPTIONS_EXTRA) $(LATEXMK_OPTIONS) $<

clean:
	$(DOCKER) $(DOCKER_COMMAND) $(DOCKER_MOUNT) blang/latex:ctanfull \
		$(LATEXMK) -bibtex -C main

render:
	$(DOCKER) $(DOCKER_COMMAND) $(DOCKER_MOUNT) blang/latex:ctanfull \
		make pdf
biber:
	$(DOCKER) $(DOCKER_COMMAND) $(DOCKER_MOUNT) blang/latex:ctanfull \
		biber main

debug:
	$(DOCKER) $(DOCKER_COMMAND) -it $(DOCKER_MOUNT) blang/latex:ctanfull \
		bash


