
\begin{figure}
  \centering
  \includegraphics[width=\linewidth]{images/numerical_grid}
  \caption{Grid with 2mm spacing}
\end{figure}
\section{Domain Discretization}

The first step in obtaining a numerical solution is to convert the (continuous) domain of interest into a discretized domain. In 2D Finite Difference schemes, each dicrete point on the grid, or "node", is uniquely identified by a set of indices (i,j). The neighbouring nodes are defined by incrementing or decrementing one of the indices by one \sidecite{strikwerda2004finite}.
The continuous PDE equations have been discretized into a system of linear algebraic variables, where each node has one unknown variable and is the subject of one algebraic equation, representing the relstionship between the node and its neighbours. Therefore, any dicretized domain can be expressed as a system of linear equations where the number of equations and unknowns must be equal, in a square matrix. In our problem, the domain of interest has temperature defined as $u = u(x,y)$ and is discretized into $U_{\left(i,j\right)}$ at each node, where x is the distance from the origin in the horizontal direction and y is the distance from the origin in the vertical direction.

At boundary conditions, where nodes may not have the usual amount of neighbouring nodes or may be subject to physical constraints, special equations need to be derived. These equations are still linear in form and can be used to replace a row in the matrix. In addition to the conditions placed on boundary nodes, we may have null nodes that are not part of the domain but are values that may be expressed using the indices. Such null nodes will have the value 0 and will not be incorporated in the calculation of other nodes. In the T-shaped domain under study, we have two regions filled with such null nodes. \sidecite{ozisik2016finite,causonintroductory}


\subsection{Determining Grid Spacing}


\begin{margintable}
\caption[Bounding box of domain]{Bounding box of domain}
\begin{tabular}{ c c}
\toprule
  $x_{start}$ - $x_{end}$ & 26 mm\\
  $y_{start}$ - $y_{end}$ & 24 mm\\
\bottomrule
\end{tabular}
\end{margintable}


Grid spacing in finite difference schemes defines the distance between adjacent points in the computational domain. This parameter impacts both resolution and computational demand; a finer grid offers greater detail but demands more resources, while a coarser grid is more efficient but sacrifices some accuracy. Optimal grid spacing hinges on specific problem characteristics and the equation being solved\sidenote{In particular if the problem has oscillating solutions, which are excarberated with insufficiently large grid spacing}, boundary conditions, and simulation requirements. Balancing these factors ensures accurate and efficient solutions in finite difference simulations. In our problem, we must have a minimum grid spacing of 2mm or finer.


\begin{table}[ht]
\caption[Number of nodes and resulting grid spacing]{Node number \& grid spacing}
\begin{tabular}{ c c c c c }
	\toprule
	Variable & Calculation & Result & Grid sizing & Total node number \\
	\midrule
    $n_x$ & 13*1 + 1 & 14 & $\Delta{}x$ = 2 mm & \multirow{2}{4em}{182}\\
    $n_y$ & 12*1 + 1 & 13 & $\Delta{}y$ = 2 mm \\
	\midrule
    $n_x$ & 13*2 + 1 & 27 & $\Delta{}x$ = 1 mm & \multirow{2}{4em}{675} \\
    $n_y$ & 12*2 + 1 & 25 & $\Delta{}y$ = 1 mm \\
	\midrule
    $n_x$ & 13*3 + 1 & 40 & $\Delta{}x$ = 0.67 mm & \multirow{2}{4em}{1480}\\
    $n_y$ & 12*3 + 1 & 37 & $\Delta{}y$ = 0.67 mm \\
	\bottomrule
\end{tabular}
\end{table}

\subsection{Ensuring node positioning on boundary layers}

In addition to balancing accuracy and efficiency, nodes must be placed on the domain boundary in order to capture the behavior of the underlying physical system at the boundaries; this prevents artifacts or inaccuracies from propagating into the interior of the domain \sidenote{cell-centered finite difference schemes avoid this issue by redefining their equations at the boundary layer to account for the distance.}, enabling a more faithful emulation of real-world scenarios. In order to simplify the equations for the finite difference sheme, we also enforce the equality of grid spacing in the x and y directions. In the table below, the calculation of grid spacing based on node numbers can be found, as well as examples of the numerical grid generated. The code used to generate and plot the grid can be found in \ref{lsting:numerical_grid_code}.

\newpage

\begin{widepar}
\begin{listing}
\label{lsting:numerical_grid_code}
  \caption{Code for Numerical Grid with spacing 2mm}
\begin{lstlisting}[language=Python]
# Domain Parameters
x_start, x_end, x_n = 0.0e-3, 26.0e-3, 13*1+1
y_start, y_end, y_n = 0.0e-3, 24.0e-3, 12*1+1

# Compute cell length
dx = (x_end-x_start)/(x_n-1)
dy = (y_end-y_start)/(y_n-1)

# Specify a rectangular grid
X = np.linspace(x_start, x_end, num=x_n) # mesh points in x dir
Y = np.linspace(y_start, y_end, num=y_n) # mesh points in y dir

XX, YY = np.meshgrid(X, Y, indexing="ij")
UU = np.zeros_like(XX)
MASK = (XX > 16e-3) & ( (YY < 6e-3)|(YY > 18e-3))
MASK = ~MASK

# Specify a rectangular grid
X = np.linspace(x_start, x_end, num=x_n) # mesh points in x dir
Y = np.linspace(y_start, y_end, num=y_n) # mesh points in y dir

XX, YY = np.meshgrid(X, Y, indexing="ij")
UU = np.zeros_like(XX)

# Make plot of domain
import matplotlib.pyplot as plt
fig, ax = plt.subplots(1, 1, constrained_layout=True, figsize=(8, 6),dpi=600)

# Draw discretized domain without nodes outside domain
MASK = (XX > 16e-3) & ( (YY < 6e-3)|(YY > 18e-3))
MASK = ~MASK
ax.plot(XX[MASK],  YY[MASK],  marker='.', color='k', linestyle='none')
ax.plot(XX[~MASK], YY[~MASK], marker='.', color='w', linestyle='none')


# Draw thick lines for boundary
ax.plot([ 0e-3,  0e-3], [24e-3,  0e-3], color='k', linestyle='solid')  # B1
ax.plot([ 0e-3, 16e-3], [ 0e-3,  0e-3], color='k', linestyle='solid')  # B2
ax.plot([16e-3, 16e-3], [ 0e-3,  6e-3], color='k', linestyle='solid')  # B3
ax.plot([16e-3, 26e-3], [ 6e-3,  6e-3], color='k', linestyle='solid')  # B4
ax.plot([26e-3, 26e-3], [ 6e-3, 18e-3], color='k', linestyle='solid')  # B5
ax.plot([26e-3, 16e-3], [18e-3, 18e-3], color='k', linestyle='solid')  # B6
ax.plot([16e-3, 16e-3], [18e-3, 24e-3], color='k', linestyle='solid')  # B7
ax.plot([16e-3,  0e-3], [24e-3, 24e-3], color='k', linestyle='solid')  # B8

# Plot horizontal and vertical grid lines for ease of visualization
ax.vlines(np.linspace(x_start, x_end, num=x_n),
          y_start, y_end, color='k',
          linestyle='dashed', alpha=0.27)
          ax.hlines(np.linspace(y_start, y_end, num=y_n),
          x_start, x_end, color='k',
          linestyle='dashed', alpha=0.27)

# Plot labels and set x \& y  limits on plot
ax.set_title("Discretized Domain")
ax.set_xlabel('x')
ax.set_ylabel('y')
ax.set_xlim(-2e-3, 28e-3)
ax.set_ylim(-2e-3, 26e-3)

# Plot and save figure
plt.show()
fig.savefig('numerical_grid_1mm.png')

\end{lstlisting}
\end{listing}
\end{widepar}


\begin{figure*}
  \centering
  \includegraphics[width=0.8\linewidth]{images/numerical_grid}
  \caption{Numerical Grid for Grid spacing 2 mm}
\end{figure*}

\begin{figure*}
  \centering
  \includegraphics[width=0.8\linewidth]{images/numerical_grid_1mm}
  \caption{Numerical Grid for Grid spacing 1 mm}
\end{figure*}

\begin{figure*}
  \centering
  \includegraphics[width=0.8\linewidth]{images/numerical_grid_066mm}
  \caption{Numerical Grid for Grid spacing 0.67 mm}
\end{figure*}


\begin{figure*}
  \centering
  \includegraphics[width=0.8\linewidth]{images/numerical_grid_05mm}
  \caption{Numerical Grid for Grid spacing 0.5 mm}
\end{figure*}

\FloatBarrier

\subsection{Identifying nodes on plane surface and corners}

Identifying node points on the boundary and corners in finite difference schemes is critical to apply boundary conditions, which define the behavior of a system at its edges. Applying incorrect equations to these points can cause inaccuracies in the results; pinpointing nodes on the boundary and at corners is pivotal in achieving reliable and physically meaningful solutions in finite difference simulations.


\begin{table*}[ht]
\caption[Boundary Surface and index definitions]{Boundary Surface and index definitions}
\begin{tabular}{ c c l l }
\toprule
Label & Boundary Condition & x index & y index \\
\midrule
B1 & Constant Heat Flux & $i = 1$ & $j \in \left[ 2, \text{int}\left(\dfrac{24e-3}{\Delta{x}}\right) \right]$ \\
B3 & Convection         & $i = \text{int}\left(\dfrac{16e-3}{dx}\right)+1$ & $j \in \left[ 2, \text{int}\left(\dfrac{16e-3}{\Delta{x}}\right) \right]$ \\
B5 & Constant Temperature & $i = \text{int}\left(\dfrac{26e-3}{\Delta{}x}\right)$ & $j \in \left[ \text{int}\left(\dfrac{6e-3}{\Delta{x}}\right), \text{int}\left(\dfrac{18e-3}{\Delta{x}}\right) \right]$ \\
B7 & Adiabatic & $i = \text{int}\left(\dfrac{16e-3}{\Delta{}x}\right)$ & $j \in \left[ \text{int}\left(\dfrac{18e-3}{\Delta{x}}\right), \text{int}\left(\dfrac{24e-3}{\Delta{x}}\right) \right]$ \\
\midrule
B2 & Constant Heat Flux & $i \in \left[ 2, \text{int}\left(\dfrac{16e-3}{\Delta{x}}\right) \right]$ & $j = 1$\\
B4 & Adiabatic & $i \in \left[  \text{int}\left(\dfrac{16e-3}{\Delta{x}}\right) + 1, \text{int}\left(\dfrac{26e-3}{\Delta{x}}\right) \right]$ & $j = \text{int}\left(\dfrac{6e-3}{\Delta{}x}\right)+1$ \\
B6 & Constant Temperature & $i \in \left[  \text{int}\left(\dfrac{16e-3}{\Delta{x}}\right) + 1, \text{int}\left(\dfrac{26e-3}{\Delta{x}}\right) \right]$ & $j = \text{int}\left(\dfrac{18e-3}{\Delta{}x}\right)+1$ \\
B8 & Symmetry & $i \in \left[ 1 + 1, \text{int}\left(\dfrac{26e-3}{\Delta{x}}\right) \right]$ & $j = \text{int}\left(\dfrac{18e-3}{\Delta{}x}\right)+1$ \\
\bottomrule
\end{tabular}
\end{table*}

\begin{table*}[ht]
\caption[Boundary Corner and index definitions]{Boundary Corner and index definitions}
\begin{tabular}{ c l l }
\toprule
Corner & x index & y index \\
\midrule
B12 & $i=1$                                            & $j=1$ \\
B23 & $i=\text{int}\left(\dfrac{16e-3}{\Delta{x}}\right)+1$ & $j=1$ \\
B34 & $i=\text{int}\left(\dfrac{16e-3}{\Delta{x}}\right)+1$ & $j=\text{int}\left(\dfrac{6e-3}{\Delta{x}}\right)$ \\
B45 & $i=\text{int}\left(\dfrac{26e-3}{\Delta{x}}\right)+1$ & $j=\text{int}\left(\dfrac{6e-3}{\Delta{x}}\right)$ \\
B56 & $i=\text{int}\left(\dfrac{26e-3}{\Delta{x}}\right)+1$ & $j=\text{int}\left(\dfrac{18e-3}{\Delta{x}}\right)$ \\
B67 & $i=\text{int}\left(\dfrac{16e-3}{\Delta{x}}\right)+1$ & $j=\text{int}\left(\dfrac{18e-3}{\Delta{x}}\right)$ \\
B78 & $i=\text{int}\left(\dfrac{16e-3}{\Delta{x}}\right)+1$ & $j=\text{int}\left(\dfrac{24e-3}{\Delta{x}}\right)$ \\
B81 & $i=1$                                            & $j=\text{int}\left(\dfrac{24e-3}{\Delta{x}}\right)$ \\
\bottomrule
\end{tabular}
\end{table*}



The full code for identifying boundary points can be found at \ref{lsting:numerical_grid_code}. The essential parts are reproduced here for the reader's convenience.

\begin{widepar}
\begin{listing}
\label{lsting:numerical_grid_code}
  \caption{Code for Numerical Grid with spacing 2mm}
\begin{lstlisting}[language=Python]
# B1 Hot Flux
B1 =  [(1,j)      for j in range(int(1+1), int(24e-3/dx)+1)]
B1x = [(i-1) * dx  for (i,j) in B1 ]
B1y = [(j-1) * dy for (i,j) in B1 ]
ax.plot(B1x, B1y,  marker='.', color='r',label='B1')

# B12 Corner point between Hot and Cold flux
B12 =  [(1,1)]
B12x = [(i-1) * dx for (i,j) in B12 ]
B12y = [(j-1) * dy  for (i,j) in B12 ]
ax.plot(B12x, B12y,  marker='.', color='k',markersize=10)
\end{lstlisting}
\end{listing}
\end{widepar}

\begin{figure*}
  \centering
  \includegraphics[width=0.8\linewidth]{images/numerical_grid_boundary_2mm}
  \caption{Identifying Boundary Nodes for 2 mm spacing}
\end{figure*}

\FloatBarrier

%\begin{figure*}
%  \centering
%  \includegraphics[width=0.8\linewidth]{images/numerical_grid_boundary_2mm}
%  \caption{Identifying Boundary Nodes for 2 mm spacing}
%\end{figure*}

\begin{figure*}
  \centering
  \includegraphics[width=0.8\linewidth]{images/numerical_grid_boundary_1mm}
  \caption{Identifying Boundary Nodes for 1 mm spacing}
\end{figure*}

\begin{figure*}
  \centering
  \includegraphics[width=0.8\linewidth]{images/numerical_grid_boundary_067mm}
  \caption{Identifying Boundary Nodes for 0.67 mm spacing}
\end{figure*}

%\begin{figure*}
%  \centering
%  \includegraphics[width=0.8\linewidth]{images/numerical_grid_boundary_05mm}
%  \caption{Identifying Boundary Nodes for 0.5 mm spacing}
%\end{figure*}

\FloatBarrier
