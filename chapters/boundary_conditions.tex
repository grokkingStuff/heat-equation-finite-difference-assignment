
\section{Boundary Conditions}

At boundary conditions, where nodes may not have the usual amount of neighbouring nodes or may be subject to physical constraints, special equations need to be derived. These equations are still linear in form and can be used to replace a row in the coefficient matrix describing the problem.

In addition to the conditions placed on boundary nodes, we may have null nodes that are not part of the domain but are values that may be expressed using the indices. Such null nodes will have the value 0 and will not be incorporated in the calculation of other nodes. In the T-shaped domain under study, we have two regions filled with such null nodes.

\begin{figure*}
	\includegraphics{images/boundary_conditions.png}
	\caption[Boundary Conditions]{Boundary Conditions described in problem}
	\labfig{fig:boundary_condition}
\end{figure*}

\subsection{Types of Boundary Conditions}

\subsubsection{Constant Temperature Surface}
A constant temperature boundary condition dictates that the temperature at the designated boundary remains invariant over time. In other words, there is an heat source/sink at fixed temperature with infinitely large thermal conductivity that enforces the constant temperature at the boundary. Physically, constant temperature boundary conditions can be used to model the surface of materials undergoing phase-change (boiling, freezing, sublimation).

Given that the temperature field is defined at these points, it is remarkably easy to create equations for the same.

\begin{equation}
    U_{i,j} = U_c
\end{equation}

\begin{kaobox}[frametitle=FD Stencil for Nodes at Constant Temperature Surface]

\begin{equation}
U_{i,j} = U_c
\end{equation}

\end{kaobox}


\subsubsection{Constant heat flux surface}
A constant heat flux boundary condition dictates that the heat flux at the designated boundary remains invariant over time. In other words, the heat entering or leaving the system is fixed, regardless of the temperature. Physically, constant heat flux boundary conditions can be used to model electrical resistance heaters or radiative heating from a source that is at much higher temperature than the surface.

\begin{align}
    q_{\left(i,j-1\right) \rightarrow \left( i,j \right) } &=  k \left( \frac{\Delta{x}}{2} l \right) \frac{U_{i,j-1} - U_{i,j}}{\Delta{y}} \\
    q_{\left(i,j+1\right) \rightarrow \left( i,j \right) } &=  k \left( \frac{\Delta{x}}{2} l \right) \frac{U_{i,j+1} - U_{i,j}}{\Delta{y}} \\
    q_{\left(i+1,j\right) \rightarrow \left( i,j \right) } &=  k \left( \Delta{y} l \right) \frac{U_{i+1,j} - U_{i,j}}{\Delta{x}} \\
    q_{\left(C_1\right) \rightarrow \left( i,j \right) } &=  C_1 \Delta{y} l
\end{align}

We can combine the above equations to give us:
\begin{align}
\Sigma q &= 0 \\
q_{\left(i,j-1\right) \rightarrow \left( i,j \right) } +  q_{\left(i,j+1\right) \rightarrow \left( i,j \right) } +  q_{\left(i+1,j\right) \rightarrow \left( i,j \right) } + q_{\left(C_1\right) \rightarrow \left( i,j \right) } &= 0
    %q_{\left(i+1,j\right) \rightarrow \left( i,j \right) } &=  k \left( \Delta{y} l \right) \frac{U_{i+1,j} - U_{i,j}}{\Delta{x}} \\
\end{align}
\begin{align}
\frac{k\Delta{x}l}{2\Delta{y}} U_{i,j-1} + \frac{k\Delta{x}l}{2\Delta{y}} U_{i,j+1} + \frac{k\Delta{y}l}{2\Delta{x}} U_{i+1,j} - \left[ \frac{k\Delta{x}l}{\Delta{y}} + \frac{k\Delta{x}l}{\Delta{y}} \right] U_{i,j}= - C \Delta{y}l
\end{align}

If we assume $\Delta{y} = \Delta{x}$, we can simplify the above equation to give us:

\begin{equation}
-4 U_{i,j} + U_{i,j-1} + U_{i,j+1} + 2U_{i+1,j} = \frac{-2C_1\Delta{x}}{k}
\end{equation}


We can follow a similar line of reasoning for the FD Stencil for horizontal surface nodes with constant heat flux from other directions.

\begin{kaobox}[frametitle=FD Stencil for Nodes at Surface with constant heat flux]

Nodes at vertical surface with constant heat flux from left
\begin{equation}
\label{eq:nodes_vertical_constanU_heaU_flux_left}
-4 U_{i,j} + U_{i,j-1} + U_{i,j+1} + 2U_{i+1,j} = \frac{-2C_1\Delta{x}}{k}
\end{equation}

Nodes at vertical surface with constant heat flux from right
\begin{equation}
\label{eq:nodes_vertical_constanU_heaU_flux_right}
-4 U_{i,j} + U_{i,j-1} + U_{i,j+1} + 2U_{i-1,j} = \frac{-2C_2\Delta{x}}{k}
\end{equation}

Nodes at horizontal surface with constant heat flux from bottom
\begin{equation}
\label{eq:nodes_horizontal_constanU_heaU_flux_bottom}
-4 U_{i,j} + U_{i-1,j} + U_{i+1,j} + 2U_{i,j+1} = \frac{-2C_3\Delta{x}}{k}
\end{equation}

Nodes at horizontal surface with constant heat flux from top
\begin{equation}
\label{eq:nodes_horizontal_constanU_heaU_flux_top}
-4 U_{i,j} + U_{i-1,j} + U_{i+1,j} + 2U_{i,j-1} = \frac{-2C_4\Delta{x}}{k}
\end{equation}

\end{kaobox}

\subsubsection{Adiabatic surface}
Adiabatic boundary condition dictates that no heat flows across the boundary. In other words, it is a perfect insulator that has a constant heat flux of zero. Physically, adiabatic boundaries can be used to model a well-insulated surface.

We can reuse the previous results \eqref{eq:nodes_vertical_constanU_heaU_flux_left}, \eqref{eq:nodes_vertical_constanU_heaU_flux_right}, \eqref{eq:nodes_horizontal_constanU_heaU_flux_bottom}, \eqref{eq:nodes_horizontal_constanU_heaU_flux_top} while setting the heat flux term to zero.

\begin{kaobox}[frametitle=FD Stencil for Nodes at Adiabatic surface]

Nodes at vertical surface with zero heat flux at left
\begin{equation}
\label{eq:nodes_vertical_adiabatic_left}
-4 U_{i,j} + U_{i,j-1} + U_{i,j+1} + 2U_{i+1,j} = 0
\end{equation}

Nodes at vertical surface with zero heat flux at right
\begin{equation}
\label{eq:nodes_vertical_adiabatic_right}
-4 U_{i,j} + U_{i,j-1} + U_{i,j+1} + 2U_{i-1,j} = 0
\end{equation}

Nodes at horizontal surface with zero heat flux at bottom
\begin{equation}
\label{eq:nodes_horizontal_adiabatic_bottom}
-4 U_{i,j} + U_{i-1,j} + U_{i+1,j} + 2U_{i,j+1} = 0
\end{equation}

Nodes at horizontal surface with zero heat flux at top
\begin{equation}
\label{eq:nodes_horizontal_adiabatic_top}
-4 U_{i,j} + U_{i-1,j} + U_{i+1,j} + 2U_{i,j-1} = 0
\end{equation}

\end{kaobox}

\subsubsection{Symmetric Boundary Condition}
A symmetric boundary condition dictates that the same physical processes exist on both sides of the boundary. This condition is used when a physical model is symmetric such that all variables have the same value and gradients across a boundary and it would be computationally cheaper to only model one half of it. It can be modeled as an adiabatic condition (zero heat flux).

\begin{kaobox}[frametitle=FD Stencil for Nodes at Symmetry surface]

Nodes at vertical symmetry surface at left
\begin{equation}
\label{eq:nodes_vertical_symmetry_left}
-4 U_{i,j} + U_{i,j-1} + U_{i,j+1} + 2U_{i+1,j} = 0
\end{equation}

Nodes at vertical symmetry surface at right
\begin{equation}
\label{eq:nodes_vertical_symmetry_right}
-4 U_{i,j} + U_{i,j-1} + U_{i,j+1} + 2U_{i-1,j} = 0
\end{equation}

Nodes at vertical symmetry surface at bottom
\begin{equation}
\label{eq:nodes_horizontal_symmetry_bottom}
-4 U_{i,j} + U_{i-1,j} + U_{i+1,j} + 2U_{i,j+1} = 0
\end{equation}

Nodes at vertical symmetry surface at top
\begin{equation}
\label{eq:nodes_horizontal_symmetry_top}
-4 U_{i,j} + U_{i-1,j} + U_{i+1,j} + 2U_{i,j-1} = 0
\end{equation}

\end{kaobox}

\subsubsection{Convection Surface}

A convection boundary condition occurs when the surface is exposed to convective heat transfer, with the temperature of the fluid often specified as a constant free stream temperature. Physically, convection boundaries can be used to model realistic heat sinks that use water or other fluids to wick away heat, as well as boilers that add heat through hot steam.

\begin{align}
    q_{\left(i,j-1\right) \rightarrow \left( i,j \right) } &=  k \left( \frac{\Delta{x}}{2} l \right) \frac{U_{i,j-1} - U_{i,j}}{\Delta{y}} \\
    q_{\left(i,j+1\right) \rightarrow \left( i,j \right) } &=  k \left( \frac{\Delta{x}}{2} l \right) \frac{U_{i,j+1} - U_{i,j}}{\Delta{y}} \\
    q_{\left(i+1,j\right) \rightarrow \left( i,j \right) } &=  k \left( \Delta{y} l \right) \frac{U_{i+1,j} - U_{i,j}}{\Delta{x}} \\
    q_{\left( \infty \right) \rightarrow \left( i,j \right) } &=  h \left( \Delta{y} l \right) \left( U_{\infty} - U_{i,j} \right)
\end{align}

We can combine the above equations to give us:
\begin{align}
\Sigma q &= 0 \\
q_{\left(i,j-1\right) \rightarrow \left( i,j \right) } +  q_{\left(i,j+1\right) \rightarrow \left( i,j \right) } +  q_{\left(i+1,j\right) \rightarrow \left( i,j \right) } + q_{\left(\infty\right) \rightarrow \left( i,j \right) } &= 0
    %q_{\left(i+1,j\right) \rightarrow \left( i,j \right) } &=  k \left( \Delta{y} l \right) \frac{U_{i+1,j} - U_{i,j}}{\Delta{x}} \\
\end{align}
\begin{align}
\frac{k\Delta{x}l}{2\Delta{y}} U_{i,j-1} + \frac{k\Delta{x}l}{2\Delta{y}} U_{i,j+1} + \frac{k\Delta{y}l}{\Delta{x}} U_{i+1,j} - \left[ \frac{k\Delta{x}l}{\Delta{y}} + \frac{k\Delta{x}l}{\Delta{y}} + h\Delta{y}l \right] U_{i,j}= - h\Delta{y}l U_{\infty}
\end{align}

If we assume $\Delta{y} = \Delta{x}$, we can simplify the above equation to give us:

\begin{equation}
-\left[ 2 + \frac{2 \Delta{x} h}{k}\right] U_{i,j} + U_{i,j-1} + U_{i,j+1} + 2U_{i+1,j} = \frac{2 \Delta{x} h U_{\infty}}{k}
\end{equation}

We can follow a similar line of reasoning for the FD Stencil for horizontal surface nodes with constant heat flux from other directions.



\begin{kaobox}[frametitle=FD Stencil for Nodes at Symmetry surface]

Nodes at vertical surface with convection at left
\begin{equation}
\label{eq:nodes_vertical_convection_left}
-\left[ 2 + \frac{2 \Delta{x} h}{k}\right] U_{i,j} + U_{i,j-1} + U_{i,j+1} + 2U_{i+1,j} = \frac{2 \Delta{x} h U_{\infty}}{k}
\end{equation}

Nodes at vertical surface with convection at right
\begin{equation}
\label{eq:nodes_vertical_convection_right}
-\left[ 2 + \frac{2 \Delta{x} h}{k}\right] U_{i,j} + U_{i,j-1} + U_{i,j+1} + 2U_{i-1,j} = \frac{2 \Delta{x} h U_{\infty}}{k}
\end{equation}

Nodes at vertical surface with convection at bottom
\begin{equation}
\label{eq:nodes_horizontal_convection_bottom}
-\left[ 2 + \frac{2 \Delta{x} h}{k}\right] U_{i,j} + U_{i-1,j} + U_{i+1,j} + 2U_{i,j+1} = \frac{2 \Delta{x} h U_{\infty}}{k}
\end{equation}

Nodes at vertical surface with convection at top
\begin{equation}
\label{eq:nodes_horizontal_convection_top}
-\left[ 2 + \frac{2 \Delta{x} h}{k}\right] U_{i,j} + U_{i-1,j} + U_{i+1,j} + 2U_{i,j-1} = \frac{2 \Delta{x} h U_{\infty}}{k}
\end{equation}

\end{kaobox}

\subsection{Nodes at corner of boundary conditions}

\subsubsection{Exterior South-West Corner with South Heat Flux and West Heat Flux}

\begin{align}
    q_{\left(i,j+1\right) \rightarrow \left( i,j \right) } &=  k \left( \frac{\Delta{x}}{2} l \right) \frac{U_{i,j+1} - U_{i,j}}{\Delta{y}} \\
    q_{\left(i+1,j\right) \rightarrow \left( i,j \right) } &=  k \left( \frac{\Delta{y}}{2} l \right) \frac{U_{i+1,j} - U_{i,j}}{\Delta{x}} \\
    q_{\left(C_{hot}\right) \rightarrow \left( i,j \right) } &=  C_{hot} \frac{\Delta{y} l}{2}   \\
    q_{\left(C_{cold}\right) \rightarrow \left( i,j \right) } &=  C_{cold} \frac{\Delta{x} l}{2}
\end{align}

We can combine the above equations to give us:
\begin{align}
\Sigma q &= 0 \\
q_{\left(i,j+1\right) \rightarrow \left( i,j \right) } +  q_{\left(i+1,j\right) \rightarrow \left( i,j \right) }  +  q_{\left(C_{hot}\right) \rightarrow \left( i,j \right) }   + q_{\left(C_{cold}\right) \rightarrow \left( i,j \right) }  &= 0
    %q_{\left(i+1,j\right) \rightarrow \left( i,j \right) } &=  k \left( \Delta{y} l \right) \frac{U_{i+1,j} - U_{i,j}}{\Delta{x}} \\
\end{align}
\begin{align}
\frac{k\Delta{x}l}{2\Delta{y}} U_{i,j+1} + \frac{k\Delta{y}l}{2\Delta{x}} U_{i+1,j} -
\left[ \frac{k\Delta{x}l}{2\Delta{y}} + \frac{k\Delta{y}l}{2\Delta{x}} \right] U_{i,j} = - C_{hot} \frac{\Delta{y}l}{2} - C_{cold} \frac{\Delta{x}l}{2}
\end{align}

If we assume $\Delta{y} = \Delta{x}$, we can simplify the above equation to give us:

%\begin{kaobox}[frametitle=FD Stencil for Exterior South-West Corner with South Cold Flux and West Hot Flux]
\begin{equation}
-2 U_{i,j} + U_{i,j+1} + U_{i+1,j} + = \frac{-C_{hot}\Delta{x}}{2k} + \frac{-C_{cold}\Delta{x}}{2k}
\end{equation}
%\end{kaobox}

\subsubsection{Exterior North-West Corner with North Symmetry and West Hot Flux}

\begin{align}
    q_{\left(i,j-1\right) \rightarrow \left( i,j \right) } &=  k \left( \frac{\Delta{x}}{2} l \right) \frac{U_{i,j-1} - U_{i,j}}{\Delta{y}} \\
    q_{\left(i+1,j\right) \rightarrow \left( i,j \right) } &=  k \left( \frac{\Delta{y}}{2} l \right) \frac{U_{i+1,j} - U_{i,j}}{\Delta{x}} \\
    q_{\left(\text{symm}\right) \rightarrow \left( i,j \right) } &=  0 \\
    q_{\left(C\right) \rightarrow \left( i,j \right) } &=  C_1 \frac{\Delta{y} l}{2}
\end{align}

We can combine the above equations to give us:
\begin{align}
\Sigma q &= 0 \\
q_{\left(i,j-1\right) \rightarrow \left( i,j \right) } +  q_{\left(i+1,j\right) \rightarrow \left( i,j \right) }  +  q_{\left(\text{symm}\right) \rightarrow \left( i,j \right) }  + q_{\left(C\right) \rightarrow \left( i,j \right) }  &= 0
    %q_{\left(i+1,j\right) \rightarrow \left( i,j \right) } &=  k \left( \Delta{y} l \right) \frac{U_{i+1,j} - U_{i,j}}{\Delta{x}} \\
\end{align}
\begin{align}
\frac{k\Delta{x}l}{2\Delta{y}} U_{i,j-1} + \frac{k\Delta{y}l}{2\Delta{x}} U_{i+1,j} -
\left[ \frac{k\Delta{x}l}{2\Delta{y}} + \frac{k\Delta{y}l}{2\Delta{x}} \right] U_{i,j} = - C \frac{\Delta{y}l}{2}
\end{align}

If we assume $\Delta{y} = \Delta{x}$, we can simplify the above equation to give us:

%\begin{kaobox}[frametitle=FD Stencil for Exterior North-West Corner with North Symmetry and West Hot Flux]
\begin{equation}
-2 U_{i,j} + U_{i,j-1} + U_{i+1,j} = \frac{-C\Delta{x}}{k}
\end{equation}
%\end{kaobox}


\subsubsection{Exterior North-East Corner with North Symmetry and East Adiabatic condition}

\begin{align}
    q_{\left(i,j-1\right) \rightarrow \left( i,j \right) } &=  k \left( \frac{\Delta{x}}{2} l \right) \frac{U_{i,j-1} - U_{i,j}}{\Delta{y}} \\
    q_{\left(i-1,j\right) \rightarrow \left( i,j \right) } &=  k \left( \frac{\Delta{y}}{2} l \right) \frac{U_{i-1,j} - U_{i,j}}{\Delta{x}} \\
    q_{\left(\text{symm}\right) \rightarrow \left( i,j \right) } &=  0 \\
    q_{\left(\text{adia}\right) \rightarrow \left( i,j \right) } &= 0
\end{align}

We can combine the above equations to give us:
\begin{align}
\Sigma q &= 0 \\
q_{\left(i,j-1\right) \rightarrow \left( i,j \right) } +  q_{\left(i-1,j\right) \rightarrow \left( i,j \right) }  +  q_{\left(\text{symm}\right) \rightarrow \left( i,j \right) }  +q_{\left(\text{adia}\right) \rightarrow \left( i,j \right) }  &= 0
\end{align}
\begin{align}
\frac{k\Delta{x}l}{2\Delta{y}} U_{i,j-1} + \frac{k\Delta{y}l}{2\Delta{x}} U_{i-1,j} -
\left[ \frac{k\Delta{x}l}{2\Delta{y}} + \frac{k\Delta{y}l}{2\Delta{x}} \right] U_{i,j} = 0
\end{align}

If we assume $\Delta{y} = \Delta{x}$, we can simplify the above equation to give us:

\begin{equation}
-2 U_{i,j} + U_{i,j-1} + U_{i-1,j} = 0
\end{equation}

\subsubsection{Interior South-West Corner with South Const Temperature and West Adiabatic condition}

The node is the interior south-west corner is forced to both stay at constant temperature and avoid heat transfer through the adiabatic surface. As the node has other means of heat conduction, this is not a contradiction. The constant temperature condition shall be applied here.

\begin{equation}
U_{i,j}  = U_c
\end{equation}

\subsubsection{Exterior North-East Corner with North Const Temperature and East Const Temperature}

The constant temperature condition shall be applied to the node here.

\begin{equation}
U_{i,j}  = U_c
\end{equation}

\subsubsection{Exterior South-East Corner with South Adiabatic and East Constant Temperature}

The node is the exterior south-east corner is forced to both stay at constant temperature and avoid heat transfer through the adiabatic surface. As the node has other means of heat conduction, this is not a contradiction. The constant temperature condition shall be applied here.

\begin{equation}
U_{i,j}  = U_c
\end{equation}

\subsubsection{Interior North-West Corner with North Adiabatic and West Convection}

\begin{align}
    q_{\left(i,j+1\right) \rightarrow \left( i,j \right) } &=  k \left( \Delta{x} l \right) \frac{U_{i,j+1} - U_{i,j}}{\Delta{y}} \\
    q_{\left(i-1,j\right) \rightarrow \left( i,j \right) } &=  k \left( \Delta{y} l \right) \frac{U_{i-1,j} - U_{i,j}}{\Delta{x}} \\
    q_{\left(i,j-1\right) \rightarrow \left( i,j \right) } &=  k \left( \frac{\Delta{x} l}{2} \right) \frac{U_{i,j-1} - U_{i,j}}{\Delta{y}} \\
    q_{\left(i+1,j\right) \rightarrow \left( i,j \right) } &=  k \left( \frac{\Delta{y} l}{2} \right) \frac{U_{i+1,j} - U_{i,j}}{\Delta{x}} \\
    q_{\left(\text{adia}\right) \rightarrow \left( i,j \right) } &= 0 \\
    q_{\left(\infty\right) \rightarrow \left( i,j \right) } &= h \frac{\Delta{y}l}{2} \left( U_\infty - U_{i,j}\right)
\end{align}

We can combine the above equations to give us:
\begin{align}
\Sigma q &= 0 \\
q_{\left(i,j+1\right) \rightarrow \left( i,j \right) } +  q_{\left(i-1,j\right) \rightarrow \left( i,j \right) } &+ \\ q_{\left(i,j-1\right) \rightarrow \left( i,j \right) }  + q_{\left(i+1,j\right) \rightarrow \left( i,j \right) } &+ \\
q_{\left(\text{adia}\right) \rightarrow \left( i,j \right) } +   q_{\left(\infty\right) \rightarrow \left( i,j \right) } &= 0
\end{align}

\begin{align}
\frac{k\Delta{x}l}{\Delta{y}} U_{i,j+1} + \frac{k\Delta{y}l}{\Delta{x}} U_{i-1,j} + \frac{k\Delta{x}l}{2\Delta{y}} U_{i+1,j} + \frac{k\Delta{y}l}{2\Delta{x}} U_{i,j-1} -
\left[ \frac{3k\Delta{x}l}{2\Delta{y}} + \frac{3k\Delta{y}l}{2\Delta{x}} \right] U_{i,j} = \frac{h\Delta{x}lU_{\infty}}{2}
\end{align}

If we assume $\Delta{y} = \Delta{x}$, we can simplify the above equation to give us:

\begin{equation}
\left[-3  + \frac{h\Delta{x}}{k}\right] U_{i,j}+ U_{i,j+1} + U_{i-1,j} + 2U_{i+1,j} + 2U_{i,j-1} = \frac{-h\Delta{x}U_{\infty}}{k}
\end{equation}


\subsubsection{Exterior South-East Corner with South Heat Flux and East Convection}

\begin{align}
    q_{\left(i,j+1\right) \rightarrow \left( i,j \right) } &=  k \left( \frac{\Delta{x}}{2} l \right) \frac{U_{i,j+1} - U_{i,j}}{\Delta{y}} \\
    q_{\left(i-1,j\right) \rightarrow \left( i,j \right) } &=  k \left( \frac{\Delta{y}}{2} l \right) \frac{U_{i-1,j} - U_{i,j}}{\Delta{x}} \\
    q_{\left(C_{cold}\right) \rightarrow \left( i,j \right) } &=  C_{cold} \frac{\Delta{x} l}{2} \\
    q_{\left(\infty\right) \rightarrow \left( i,j \right) } &= h \frac{\Delta{y}l}{2} \left( U_\infty - U_{i,j}\right)  \\
\end{align}

We can combine the above equations to give us:
\begin{align}
\Sigma q &= 0 \\
q_{\left(i,j+1\right) \rightarrow \left( i,j \right) } +  q_{\left(i-1,j\right) \rightarrow \left( i,j \right) }  +  q_{\left(C_{cold}\right) \rightarrow \left( i,j \right) }   + q_{\left(\infty\right) \rightarrow \left( i,j \right) } &= 0
    %q_{\left(i+1,j\right) \rightarrow \left( i,j \right) } &=  k \left( \Delta{y} l \right) \frac{U_{i+1,j} - U_{i,j}}{\Delta{x}} \\
\end{align}
\begin{align}
\frac{k\Delta{x}l}{2\Delta{y}} U_{i,j+1} + \frac{k\Delta{y}l}{2\Delta{x}} U_{i+1,j} -
\left[ \frac{k\Delta{x}l}{2\Delta{y}} + \frac{k\Delta{y}l}{2\Delta{x}} + \frac{-h\Delta{x}l}{2}  \right] U_{i,j} = - C_{cold} \frac{\Delta{x}l}{2} + \frac{-h\Delta{x}lU_{\infty}}{2}
\end{align}

If we assume $\Delta{y} = \Delta{x}$, we can simplify the above equation to give us:

%\begin{kaobox}[frametitle=FD Stencil for Exterior South-West Corner with South Cold Flux and West Hot Flux]
\begin{equation}
-\left[2 + \frac{h\Delta{x}}{k} \right]U_{i,j} + U_{i,j+1} + U_{i+1,j} + = \frac{-C_{cold}\Delta{x}}{2k} + \frac{-h\Delta{x}U_{\infty}}{k}
\end{equation}
%\end{kaobox}
