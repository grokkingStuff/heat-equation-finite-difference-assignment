
\section{Introduction}

Finite difference methods are an elegant way to solve systems differential equations; a domain of interest is divided into a set of discrete points\sidenote{Other numerical methods, such as finite element and finite volume, also aim to model a continuous field problem with a discretized field with finite nodes but FDM approximates the difference as node values on a grid while FEM and FVM use interpolation or Galerkin methods to approximate a field. In addition to being simpler to understand, FDM is embarassingly parallel while FDM has quadratic complexity in scaling}, spatial and time derivatives are approximated with their finite difference equivalents, and the partial differential equations are converted into a system of simultaneous linear equations. The solution of these equations is the approximate solution of the original boundary value problem. \sidecite{langtangen2017finite, Tezuka2006}

\begin{marginfigure}
	\includegraphics{images/heat_conduction_candle}
	\caption[Example of Heat Conduction]{Example of Heat Conduction. Courtesy of MikeRat at WikiMedia Commons\cite{heat_conduction_candle}. }
	\labfig{fig:heat_conduction_candle}
\end{marginfigure}

The heat conduction problem is an example of an elliptic partial differential equation, and can be expressed with as Eq. \eqref{poisson-eq}

\begin{align}
  \label{poisson-eq}
 \frac{\partial u}{\partial t}  - \nabla^2 u = q_{gen}
\end{align}

where $u = u(x,y,t)$ is the temperature field at time t, at x distance from the origin in the horizontal direction, at y distance from the origin in the vertical direction. The above problem is studied in $\Omega$ is the domain of interest, in which we need to specify boundary conditions for $u$ defined on the domain boundary $\partial \Omega$.

In the case of two-dimensional heat conduction, Eq \eqref{poisson-eq} can be simplified into:

If we assume steady state, constant properties, and no local energy generation, we can simplify \eqref{poisson-eq} into \eqref{heat-eq}

\begin{align}
  \label{heat-eq}
  - \nabla^2 u = \frac{\partial^2 u}{\partial x^2} + \frac{\partial^2 u}{\partial y^2} = 0
\end{align}

We can replace the partial derivatives with their central finite difference terms \sidenote{This will be explained in more detail in the next sections}:

\begin{align}
  \label{temperature_CDS}
\frac{\partial^2 u}{\partial y^2}\Bigr|_{\substack{x=x_i\\y=y_j}} \approx \frac{U_{i+1,j} - 2 U_{i-1,j} + U_{i-1,j}}{\Delta x} \\
\frac{\partial^2 u}{\partial y^2}\Bigr|_{\substack{x=x_i\\y=y_j}} \approx \frac{U_{i+1,j} - 2 U_{i-1,j} + U_{i-1,j}}{\Delta y}
\end{align}

to get the following explicit equation for the temperature at node $U_{i,j}$:

\begin{equation}
U_{i,j} = \frac{{\left(\Delta{x}\right)}^2{\left(\Delta{y}\right)}^2}{2\left({\left(\Delta{x}\right)}^2 + {\left(\Delta{y}\right)}^2\right)}
\left[ \frac{U_{i+1,j}}{{(\Delta{}x)}^2} +  \frac{U_{i-1,j}}{{(\Delta{}x)}^2} +  \frac{U_{i,j+1}}{{(\Delta{}x)}^2} +  \frac{U_{i,j-1}}{{(\Delta{}x)}^2} \right]
\end{equation}

If we assume $\Delta{}x = \Delta{}y$, we can simplify the above equation to:

\begin{equation}
  \label{finite_stencil_equation}
    U_{i,j} = \frac{1}{4} \left[ U_{i+1,j} + U_{i-1,j} + U_{i,j+1} + U_{i,j-1} \right]
\end{equation}

Equation \eqref{finite_stencil_equation} can be understood as enforcing the temperature of a node to be the average of its four neighbouring cells. \sidenote{If we were to explore the time-based behaviour of heat conduction, we would see peaks of temperature get averages and "diffuse" into nearby cells, hence why the heat conduction equation is often described as a diffusion equation (as opposed to a convection equation).}

In summary, translating a partial differential equation (PDE) into its corresponding finite difference allows us to explore the behavior of complex physical systems, albeit approximately. This process empowers us to harness the power of computational methods to tackle a wide range of real-world problems, from fluid dynamics to heat conduction and beyond. In the subsequent sections, we will delve deeper into the intricacies of finite difference schemes, exploring their implementation, analysis, and applications in case of two-dimensional heat conduction.
