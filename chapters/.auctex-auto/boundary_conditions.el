(TeX-add-style-hook
 "boundary_conditions"
 (lambda ()
   (LaTeX-add-labels
    "eq:nodes_vertical_constanU_heaU_flux_left"
    "eq:nodes_vertical_constanU_heaU_flux_right"
    "eq:nodes_horizontal_constanU_heaU_flux_bottom"
    "eq:nodes_horizontal_constanU_heaU_flux_top"
    "eq:nodes_vertical_adiabatic_left"
    "eq:nodes_vertical_adiabatic_right"
    "eq:nodes_horizontal_adiabatic_bottom"
    "eq:nodes_horizontal_adiabatic_top"
    "eq:nodes_vertical_symmetry_left"
    "eq:nodes_vertical_symmetry_right"
    "eq:nodes_horizontal_symmetry_bottom"
    "eq:nodes_horizontal_symmetry_top"
    "eq:nodes_vertical_convection_left"
    "eq:nodes_vertical_convection_right"
    "eq:nodes_horizontal_convection_bottom"
    "eq:nodes_horizontal_convection_top"))
 :latex)

