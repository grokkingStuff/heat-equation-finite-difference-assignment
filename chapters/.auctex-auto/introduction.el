(TeX-add-style-hook
 "introduction"
 (lambda ()
   (LaTeX-add-labels
    "poisson-eq"
    "heat-eq"
    "temperature_CDS"
    "finite_stencil_equation"))
 :latex)

