\documentclass[
	fontsize=10pt, % Base font size
	twoside=false, % Use different layouts for even and odd pages (in particular, if twoside=true, the margin column will be always on the outside)
	%open=any, % If twoside=true, uncomment this to force new chapters to start on any page, not only on right (odd) pages
	%chapterprefix=true, % Uncomment to use the word "Chapter" before chapter numbers everywhere they appear
	%chapterentrydots=true, % Uncomment to output dots from the chapter name to the page number in the table of contents
	numbers=noenddot, % Comment to output dots after chapter numbers; the most common values for this option are: enddot, noenddot and auto (see the KOMAScript documentation for an in-depth explanation)
	%draft=true, % If uncommented, rulers will be added in the header and footer
	%overfullrule=true, % If uncommented, overly long lines will be marked by a black box; useful for correcting spacing problems
]{kaohandt}

% Choose the language
\usepackage[english]{babel} % Load characters and hyphenation
\usepackage[english=british]{csquotes}	% English quotes

% Load the bibliography package
\usepackage{kaobiblio}
\addbibresource{main.bib} % Bibliography file

% Load mathematical packages for theorems and related environments
\usepackage{kaotheorems}

% Load the package for hyperreferences
\usepackage{kaorefs}

% Set the paths where to look for images
\usepackage{graphicx}
\graphicspath{images/}
\usepackage{subcaption}

% Custom packages
\usepackage{blindtext}
\usepackage{color,xcolor}
\usepackage{tikz-qtree, tikz}
\usepackage[utf8]{inputenc}
\usetikzlibrary{decorations.pathreplacing,arrows,shapes,positioning,shadows,calc}
\usetikzlibrary{decorations, decorations.text,backgrounds}
\usepackage[section]{placeins}

\usepackage{color}

\definecolor{mygreen}{rgb}{0,0.6,0}
\definecolor{mygray}{rgb}{0.5,0.5,0.5}
\definecolor{mymauve}{rgb}{0.58,0,0.82}
\lstset{
  backgroundcolor=\color{white},   % choose the background color; you must add \usepackage{color} or \usepackage{xcolor}; should come as last argument
  basicstyle=\footnotesize,        % the size of the fonts that are used for the code
  breakatwhitespace=false,         % sets if automatic breaks should only happen at whitespace
  breaklines=true,                 % sets automatic line breaking
  captionpos=b,                    % sets the caption-position to bottom
  commentstyle=\color{mygreen},    % comment style
  deletekeywords={...},            % if you want to delete keywords from the given language
  escapeinside={\%*}{*)},          % if you want to add LaTeX within your code
  extendedchars=true,              % lets you use non-ASCII characters; for 8-bits encodings only, does not work with UTF-8
  firstnumber=1000,                % start line enumeration with line 1000
  frame=single,	                   % adds a frame around the code
  keepspaces=true,                 % keeps spaces in text, useful for keeping indentation of code (possibly needs columns=flexible)
  keywordstyle=\color{blue},       % keyword style
  language=Python,                 % the language of the code
  morekeywords={*,...},            % if you want to add more keywords to the set
  numbers=left,                    % where to put the line-numbers; possible values are (none, left, right)
  numbersep=5pt,                   % how far the line-numbers are from the code
  numberstyle=\tiny\color{mygray}, % the style that is used for the line-numbers
  rulecolor=\color{black},         % if not set, the frame-color may be changed on line-breaks within not-black text (e.g. comments (green here))
  showspaces=false,                % show spaces everywhere adding particular underscores; it overrides 'showstringspaces'
  showstringspaces=false,          % underline spaces within strings only
  showtabs=false,                  % show tabs within strings adding particular underscores
  stepnumber=2,                    % the step between two line-numbers. If it's 1, each line will be numbered
  stringstyle=\color{mymauve},     % string literal style
  tabsize=2,	                   % sets default tabsize to 2 spaces
  title=\lstname                   % show the filename of files included with \lstinputlisting; also try caption instead of title
}


\begin{document}

\title{Two-dimensional steady state Heat Conduction using Finite Difference Schemes}
\author[VPK]{Vishakh Pradeep Kumar}
\date{27 October 2023}
\maketitle

%\blindtext
%\input{sections/introduction.tex}
%\input{sections/data.tex}
%\input{sections/results.tex}
%\input{sections/discussion.tex}


\input{chapters/introduction}
\newpage
\input{chapters/domain_discretization}

\newpage
\input{chapters/spatial_operators}

%\newpage
%\input{chapters/boundary_conditions}

\newpage




\section{Numerical Solution and Results}

\begin{figure*}
  \centering
  \label{fig:contour_maps}
  \includegraphics[width=\linewidth]{images/solution_contour}
  \caption{Contour Map for different grid spacing}
\end{figure*}


\begin{figure*}
  \centering
  \includegraphics[width=0.8\linewidth]{images/solution_contour_2mm}
  \caption{Contour Map for 2mm spacing}
\end{figure*}

\begin{figure*}
  \centering
  \includegraphics[width=0.8\linewidth]{images/solution_contour_1mm}
  \caption{Contour Map for 1mm spacing}
\end{figure*}


\begin{figure*}
  \centering
  \includegraphics[width=0.8\linewidth]{images/solution_contour_067mm}
  \caption{Contour Map for 0.67 mm spacing}
\end{figure*}

\begin{figure*}
  \centering
  \includegraphics[width=0.8\linewidth]{images/solution_contour_05mm}
  \caption{Contour Map for 0.5 mm spacing}
\end{figure*}


\begin{figure*}
  \centering
  \includegraphics[height=0.98\linewidth,angle=-90]{images/solution_contour_04mm}
  \caption{Contour Map for 0.4mm spacing}
\end{figure*}

% \subsection{Brief Recap of Methodology}

The steps followed to achieve the previous results are: Domain Discretization, PDE Discretization, Boundary Condition derivations, and Numerical code. As can be expected from the boundary conditions, the heat enters the system from boundaries B1, B5, \& B6 and leaves the system through boundaries B2 and B3, resulting in a temperature contour with decreasing magnitude in the south-east direction.

\subsection{Physical feasibility}
In addition to the above characterization of the temperature contour, it is important to check if the solution is physically feasible; the solutions are meant to be representative of heat conduction in the physical world.

\subsubsection{Convection Boundary Temperature Gradient}
At the convection boundary, the temperature remains consistently higher than that of the surrounding fluid, keeping in line with our characterization of heat exiting the system through this specific boundary.


\subsubsection{Behavior near Adiabatic and Symmetric Boundaries}

As temperature contour lines approach adiabatic and symmetric boundary conditions, they should intersect these boundaries perpendicularly. The temperature contour lines in our results appear to be perpendicular to adiabatic and symmetric boundary conditions, and are an indication of the validity of our solution.


\begin{marginfigure}
  \centering
  \label{fig:solution_contour_perpendicular_04mm}
  \includegraphics[width=0.8\textwidth]{solution_contour_perpendicular_04mm}
  \caption{Contour Map near adiabatic and symmetric boundaries with contour temperature difference of 1 C}
\end{marginfigure}

\subsubsection{Stability near Constant Temperature Boundaries}

Proximity to constant temperature boundary conditions manifests in the temperatures of adjacent nodes hovering around 120°C. These nodes do not exhibit anomalous spikes or artifacts due to numerical artifacts or erroneous implementation

\begin{marginfigure}
  \centering
  \label{fig:solution_contour_constant_temperature_04mm}
  \includegraphics[width=0.8\textwidth]{solution_contour_constant_temperature_04mm}
  \caption{Contour Map near constant temperature regions with contour temperature difference of 1 C}
\end{marginfigure}

\subsection{Temperature Plots}

As we increase the grid resolution, the the number of nodes increases and the solution becomes more refined. The solution does not change in characteristic and appears to converge with increasing grid resolution.

\begin{figure}
  \centering
  \includegraphics[width=0.8\linewidth]{solution_hot_flux_boundary}
  \caption{Temperature at boundary B1}
\end{figure}

\begin{figure}
  \centering
  \includegraphics[width=0.8\linewidth]{solution_cold_flux_boundary}
  \caption{Temperature at boundary B2}
\end{figure}

\begin{figure}
  \centering
  \includegraphics[width=0.8\linewidth]{solution_B8_boundary}
  \caption{Temperature at boundary B8}
\end{figure}

\subsection{Energy Balance}

The energy flux at the boundaries is calculated and shown in the below figure. The net energy flux is not zero but is close to zero in comparison to the input and output energy fluxes. This is likely due to the coarsenss of the numerical grid, in addition to finite difference schemes lacking the ability to conserve energy, even if the finite difference derivations are based on conservation of energy.

\begin{figure}
  \centering
  \includegraphics[width=0.8\linewidth]{solution_bar}
  \caption{Energy Flux at boundaries}
\end{figure}


%\subsection{Emphasis on Key Findings}

%\subsection{Accuracy and Convergence}
%A. Error Analysis
%B. Convergence Rates
%C. Comparison with Analytical Solutions

%\subsection{Sensitivity to Grid Resolution}
%A. Impact of Grid Spacing
%B. Validation of Optimal Resolution
%C. Trade-offs between Accuracy and Computational Cost
%V. Stability and Numerical Artifacts
%A. Analysis of Stability Criteria
%B. Identification of Potential Artifacts
%C. Mitigation Strategies

%\subsection{Computational Efficiency and Resource Demands}
%A. Performance Metrics and Scalability
%B. Resource Requirements for Various Grid Sizes
%C. Recommendations for Optimal Utilization

%\subsection{Improvements \& Future Directions}



%X. Future Directions and Recommendations
%A. Potential Extensions of the Study
%B. Suggestions for Further Research Endeavors
%C. Practical Applications and Implications for the Field

\subsection{Conclusion}

In this assignment, the finite difference scheme for a T-shaped plate subject to different heat transfer boundary conditions is calculated and presented. The temperature fields show convergence with increasing grid size while the net calculated energy flux is a small percentage of the sum of the absolute values of the energy fluxes.


\defbibnote{bibnote}{Here are the references in citation order.\par\bigskip} % Prepend this text to the bibliography
\renewcommand*{\bibfont}{\small}
\printbibliography[title=Bibliography]% , prenote=bibnote]

\appendix

\input{chapters/appendix}



\end{document}
