(TeX-add-style-hook
 "Dy_plus"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("standalone" "margin=10pt")))
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("inputenc" "utf8")))
   (TeX-run-style-hooks
    "latex2e"
    "standalone"
    "standalone10"
    "color"
    "xcolor"
    "tikz-qtree"
    "tikz"
    "inputenc"))
 :latex)

