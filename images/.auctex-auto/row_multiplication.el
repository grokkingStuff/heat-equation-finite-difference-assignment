(TeX-add-style-hook
 "row_multiplication"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("xcolor" "table")))
   (TeX-run-style-hooks
    "latex2e"
    "article"
    "art10"
    "amsmath"
    "xcolor"
    "newtxtext"
    "newtxmath")
   (TeX-add-symbols
    "x"
    "y"))
 :latex)

