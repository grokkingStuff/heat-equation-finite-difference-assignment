# Heat Equation Finite Difference Assignment



## Acknowledgements

This report was built using [Federico Marotta's kaobook class](https://github.com/fmarotta/kaobook ), which was based on [Ken Arroyo
Ohori's doctoral thesis](https://3d.bk.tudelft.nl/ken/en/)


## License

This repository contains two independent works. On the one hand, the 
kaobook class, consisting of `kaobook.cls`, `kaohandt.cls`, and 
`kao.def` files plus all of the files listed in the `styles` directory; 
on the other hand, the templates and the examples in the `examples` 
directory.

The first work is licensed under the [LaTeX Project Public
License](https://www.latex-project.org/lppl/), so if you want to modify
and/or distribute the `*.cls` and `*.sty` files pertaining to this work
you have to complain with the terms of the license. However, if you just
want to use the class to compile your documents you need not worry about
the license.

The second work is released into the public domain with a Creative 
Commons Zero License.

