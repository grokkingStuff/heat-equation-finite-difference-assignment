(TeX-add-style-hook
 "main"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("kaohandt" "	fontsize=10pt" "	twoside=false" "				numbers=noenddot" "		")))
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("babel" "english") ("csquotes" "english=british") ("inputenc" "utf8") ("placeins" "section")))
   (TeX-run-style-hooks
    "latex2e"
    "chapters/introduction"
    "chapters/domain_discretization"
    "chapters/spatial_operators"
    "chapters/appendix"
    "kaohandt"
    "kaohandt10"
    "babel"
    "csquotes"
    "kaobiblio"
    "kaotheorems"
    "kaorefs"
    "graphicx"
    "subcaption"
    "blindtext"
    "color"
    "xcolor"
    "tikz-qtree"
    "tikz"
    "inputenc"
    "placeins")
   (LaTeX-add-labels
    "fig:contour_maps"
    "fig:solution_contour_perpendicular_04mm"
    "fig:solution_contour_constant_temperature_04mm")
   (LaTeX-add-bibliographies)
   (LaTeX-add-xcolor-definecolors
    "mygreen"
    "mygray"
    "mymauve"))
 :latex)

